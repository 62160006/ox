/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.phai.gameox.Player;
import com.phai.gameox.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Phai
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
//    public static void setUpClass() {
//    }
//    
//    @AfterAll
//    public static void tearDownClass() {
//    }
//    
//    @BeforeEach
//    public void setUp() {
//    }
//    
//    @AfterEach
//    public void tearDown() {
//    }

    public void testRow1ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testRow2ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testRow3ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testCol1ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testCol2ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testCol3ByX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testRow1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testRow2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testRow3ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testCol1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testCol2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testCol3ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testSlashX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testBlackSlashX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(x,table.getWinnner());
    }
    public void testSlashO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    public void testBlackSlashO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o,x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFisnish());
        assertEquals(o,table.getWinnner());
    }
    
    
    
    
    
}
