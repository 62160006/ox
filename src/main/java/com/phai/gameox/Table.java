/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.gameox;

/**
 *
 * @author Phai
 */
public class Table {

    private char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastrow;
    private int lastcol;
    private int round = 0;
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println("  1 2 3 ");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
    
    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer==playerX){
            currentPlayer = playerO;
        }else{
             currentPlayer = playerX;
        }
    }
    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatusWinLose();
    }

    private void setStatusWinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatusWinLose();
    }

    void checkBackslash() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] !=currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatusWinLose();
    }

    void checkSlash() {
        for (int row = 0, num = 2; row < 3; row++, num--) {
            if (table[row][num] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatusWinLose();
    }
    public void countRound(){
        round++;
    }
 
    void checkDraw() {
     
        if(round == 9 && finish != true){
            System.out.println("!!!Draw!!!");
            finish = true;
        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkBackslash();
        checkSlash();
        checkDraw();
    }
    public boolean isFisnish(){
        return finish;
    }
    public Player getWinnner(){
        return winner;
    }
}
