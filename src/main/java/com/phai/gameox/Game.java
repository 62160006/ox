/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.phai.gameox;

import java.util.Scanner;

/**
 *
 * @author Phai
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {

        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                table.countRound();
                break;
            }
            System.out.println("Error: table at row and col is not emply!!!");
            
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " trun");
    }

    public void run() {
        this.showWelcome();
        while(true){
            this.showTable();   
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFisnish()){
                if(table.getWinnner()==null){
                    table.showTable();
                    System.out.println("!!!Draw!!!");
                }else{
                    table.showTable();
                    System.out.println(table.getWinnner().getName()+" Win!!!");
                }
                break;
            }
            table.switchPlayer();
        }
        
        
    }
}
